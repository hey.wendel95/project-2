import { useEffect } from 'react';
import {
  useHistory,
  useLocation,
  useParams,
} from 'react-router-dom/cjs/react-router-dom.min';

export const Abc = () => {
  const { slug, id } = useParams();

  console.log(history);

  return (
    <div>
      <h1>
        {slug} {id}
      </h1>
    </div>
  );
};
